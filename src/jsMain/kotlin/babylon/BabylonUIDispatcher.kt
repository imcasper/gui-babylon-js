package babylon

import BABYLON.EventState
import BABYLON.KeyboardInfoPre
import BABYLON.PointerInfoPre
import BABYLON.Scene
import casper.scene.BabylonInputDispatcher

class BabylonUIDispatcher(scene: Scene) : BabylonInputDispatcher() {

	init {
		scene.onPreKeyboardObservable.add({ info: KeyboardInfoPre, _: EventState ->
			onKeyboard(info)
		})

		scene.onPrePointerObservable.add({ info: PointerInfoPre, _: EventState ->
			onMouse(info)
		})
	}
}