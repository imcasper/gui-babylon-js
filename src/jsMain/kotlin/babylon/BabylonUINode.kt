package babylon

import BABYLON.GUI.Container
import casper.collection.observableSetOf
import casper.core.Disposable
import casper.core.disposeAll
import casper.core.mutableDisposableListOf
import casper.geometry.Vector2d
import casper.gui.NodeInputDispatcher
import casper.gui.UIDefaultPropertyCollection
import casper.gui.UINode
import casper.gui.UIProperty
import casper.gui.component.UIComponent
import casper.gui.layout.Layout
import casper.signal.concrete.Signal
import casper.signal.concrete.SingleSignal
import casper.signal.concrete.StorageSignal
import casper.signal.util.then

class BabylonUINode(override val uiScene: BabylonUIScene, val source: Container) : UINode, Disposable {
	private val listeners = mutableDisposableListOf()

	override val propertyCollection = UIDefaultPropertyCollection()
	override val propertySignal = Signal<UIProperty>()
	override val onScene = StorageSignal<Boolean>(false)
	override val components = observableSetOf<UIComponent>()
	override val children = observableSetOf<UINode>()
	override var layout: Layout? = null
		set(value) {
			if (field == value) return
			field = value
			updateLayout()
		}

	override var mouseEventEnabled: Boolean = false
		set(value) {
			field = value
			source.isHitTestVisible = value
		}

	override var mouseEventCaptured: Boolean = false
		set(value) {
			field = value
			source.isPointerBlocker = value
		}

	override var isHitByArea: Boolean = true
		set(value) {
			field = value
			source.isHitByArea = value
		}

	override val onInvalidate = Signal<UINode>()
	override val onTransform = Signal<UINode>()
	override val onDispose = SingleSignal<UINode>()
	override val dispatcher: NodeInputDispatcher = BabylonNodeInputDispatcher(this, source)

	private var parent: BabylonUINode? = null
	private var position: Vector2d
	private var size: Vector2d
	private var preferredSize: Vector2d
	private var adapterManager = AdapterManager(uiScene, source, components)

	init {
		propertySignal.then(listeners) { property ->
			children.forEach { child ->
				child.propertySignal.set(property)
			}
		}

		position = Vector2d(source.leftInPixels.toDouble(), source.topInPixels.toDouble())
		size = Vector2d(source.widthInPixels.toDouble(), source.heightInPixels.toDouble())
		preferredSize = size

		components.then(listeners, { invalidate() }, { invalidate() })

		setClipChildren(false)

		adapterManager.update()
		updateLayout()

		children.then(listeners, ::onAdd, ::onRemove)

	}

	private var waitForUpdate = false
	override fun invalidate() {
		if (waitForUpdate) return
		waitForUpdate = true
//		uiScene.uiScreenTexture.markAsDirty()
		uiScene.scene.onBeforeRenderObservable.addOnce { _, _ ->
			onInvalidate.set(this)
			adapterManager.update()
			waitForUpdate = false
		}
	}

	override fun dispose() {
		onDispose.set(this)

		parent?.children?.remove(this)

		adapterManager.dispose()

		components.forEach {
			it.dispose()
		}
		children.forEach { it.dispose() }
		children.clear()

		source.dispose()

		listeners.disposeAll()
	}

	private fun isDeepParent(element: UINode): Boolean {
		val parent = getParent() as? BabylonUINode
		if (parent == element) return true
		if (parent == null) return false
		return parent.isDeepParent(element)
	}

	private fun onAdd(element: UINode) {
		if (element !is BabylonUINode) throw Error("Expected ${BabylonUINode::class}, can't work with: $element")
		if (element == this) throw Error("Can't add element to himself")
		if (isDeepParent(element)) throw Error("Can' add parent element as child (recursive disable)")

		source.addControl(element.source)
		element.parent = this
		updateLayout()
		element.invalidateAttachState()
	}

	private fun onRemove(element: UINode) {
		if (element !is BabylonUINode) throw Error("Expected ${BabylonUINode::class}, can't work with: $element")
		source.removeControl(element.source)
		if (element.parent == this) {
			element.parent = null
		}
		updateLayout()
		element.invalidateAttachState()
	}

	override fun getName(): String {
		return source.name ?: ""
	}

	override fun setClipChildren(value: Boolean) {
		source.clipChildren = value
		source.clipContent = value
	}

	override fun getPosition(): Vector2d {
		return position
	}

	override fun getSize(): Vector2d {
		return size
	}

	override fun getPreferredSize(): Vector2d {
		return preferredSize
	}

	override fun setPosition(position: Vector2d): UINode {
		if (this.position == position) return this
		this.position = position
		source.left = convertToPixels(position.x)
		source.top = convertToPixels(position.y)
		onTransform.set(this)
		parent?.updateLayout()
		return this
	}

	private fun updateLayout() {
		layout?.childrenLayout(preferredSize, children)
		setRealSize(layout?.selfLayout(preferredSize, children) ?: preferredSize)
	}

	private fun setRealSize(size:Vector2d) {
		if (this.size == size) return
		this.size = size

		source.width = convertToPixels(size.x)
		source.height = convertToPixels(size.y)
		onTransform.set(this)
		parent?.updateLayout()
	}

	override fun setPreferredSize(size: Vector2d): UINode {
		if (preferredSize == size) return this
		preferredSize = size
		updateLayout()
		return this
	}


	override fun getParent(): UINode? {
		return parent
	}

	private fun invalidateAttachState() {
		setAttached(isAttached())
	}

	private fun setAttached(value: Boolean) {
		onScene.set(value)
		children.forEach {
			(it as BabylonUINode).setAttached(value)
		}
	}

	private fun isAttached(): Boolean {
		var next: BabylonUINode? = this
		while (next != null) {
			if (uiScene.root === next) return true
			next = next.parent
		}
		return false
	}

	private fun convertToPixels(value: Double): String {
		return "${value}px"
	}
}