package babylon

import casper.core.Disposable
import casper.gui.UINode
import casper.gui.component.UIComponent

internal interface ComponentAdapter<T : UIComponent> : Disposable {
	val component: T
	fun invalidate()
}