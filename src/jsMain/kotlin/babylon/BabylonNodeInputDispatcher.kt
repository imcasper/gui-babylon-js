package babylon

import BABYLON.EventState
import BABYLON.GUI.Container
import BABYLON.GUI.Control
import BABYLON.GUI.Vector2WithInfo
import BABYLON.Vector2
import casper.core.DisposableHolder
import casper.core.disposeAll
import casper.core.mutableDisposableListOf
import casper.geometry.Vector2d
import casper.gui.*
import casper.input.MouseDown
import casper.input.MouseMove
import casper.input.MouseUp
import casper.scene.BabylonInputDispatcher
import casper.signal.concrete.Signal
import casper.signal.util.then

class BabylonNodeInputDispatcher(val node: UINode, source: Container) : BabylonInputDispatcher(), NodeInputDispatcher {
	private val components = mutableDisposableListOf()

	override val onMouseOut = Signal<MouseOut>()
	override val onMouseOver = Signal<MouseOver>()

	init {
		source.onPointerEnterObservable.add({ _: Control, _: EventState -> startKeyObserve() })
		source.onPointerOutObservable.add({ _: Control, _: EventState -> finishKeyObserve() })

		source.onPointerEnterObservable.add({ _: Control, _: EventState -> onMouseOver.set(MouseOver(node)) })
		source.onPointerOutObservable.add({ _: Control, _: EventState -> onMouseOut.set(MouseOut(node)) })
		source.onPointerUpObservable.add({ info: Vector2WithInfo, _: EventState ->  onMouseUp.set(MouseUp(getMouseButton(info.buttonIndex.toInt()), Vector2d(info.x, info.y), Vector2d.ZERO)) })
		source.onPointerDownObservable.add({ info: Vector2WithInfo, _: EventState -> onMouseDown.set(MouseDown(getMouseButton(info.buttonIndex.toInt()), Vector2d(info.x, info.y), Vector2d.ZERO)) })
		source.onPointerMoveObservable.add({ info: Vector2, _: EventState -> onMouseMove.set(MouseMove(Vector2d(info.x, info.y),Vector2d.ZERO)) })

		node.onDispose.then {
			finishKeyObserve()
		}
	}

	private fun finishKeyObserve() {
		components.disposeAll()
	}

	private fun startKeyObserve() {
		node.uiScene.sceneDispatcher.onMouseWheel.then(components) {if (check()) onMouseWheel.set(it)}
		node.uiScene.sceneDispatcher.onKeyDown.then(components) {if (check()) onKeyDown.set(it)}
		node.uiScene.sceneDispatcher.onKeyUp.then(components) {if (check()) onKeyUp.set(it)}
	}

	private fun check():Boolean {
		var parent = node.getParent()
		while (true) {
			if (parent == null) {
				finishKeyObserve()
				return false
			}
			if (parent == node.uiScene.root) return true
			parent = parent.getParent()
		}
	}
}