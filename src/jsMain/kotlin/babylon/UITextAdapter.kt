package babylon

import BABYLON.GUI.Container
import BABYLON.GUI.Control
import BABYLON.GUI.TextBlock
import casper.core.Disposable
import casper.format.toHexString
import casper.gui.component.text.UIText
import casper.gui.layout.Align

internal class UITextAdapter(val uiScene: BabylonUIScene, val source: Container, override val component: UIText) : Disposable, ComponentAdapter<UIText> {
	val textBlock = TextBlock("text")

	init {
		textBlock.isPointerBlocker = false
		textBlock.isHitTestVisible = false
		textBlock.verticalAlignment = Control.VERTICAL_ALIGNMENT_TOP
		textBlock.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT
		textBlock.width = "1.0"
		textBlock.height = "1.0"
		textBlock.zIndex = -1.0
		source.addControl(textBlock)
	}

	override fun dispose() {
		textBlock.dispose()
	}

	override fun invalidate() {
		textBlock.outlineWidth

		val fontProperty = component.fontProperty
		textBlock.fontSize = fontProperty.size
		textBlock.fontFamily = fontProperty.name

		val colorProperty = component.colorProperty
		textBlock.color = "#" + colorProperty.color.toHexString()

		val formatProperty = component.formatProperty
		textBlock.resizeToFit = formatProperty.resizeToFit
		textBlock.lineSpacing = formatProperty.lineSpacing
		textBlock.textWrapping = formatProperty.wrapping
		textBlock.onDirtyObservable
		val alignProperty = component.alignProperty

		textBlock.text = component.text
		if (formatProperty.resizeToFit) {
			textBlock.textHorizontalAlignment = Control.HORIZONTAL_ALIGNMENT_LEFT
			textBlock.textVerticalAlignment = Control.VERTICAL_ALIGNMENT_TOP
			val context = uiScene.uiScreenTexture.getContext()
			textBlock._link(uiScene.uiScreenTexture)
			textBlock._layout(textBlock.parent._currentMeasure, context)
			updateSize()
		} else {
			textBlock.textHorizontalAlignment = getHorizontalAlign(alignProperty.horizontalAlign)
			textBlock.textVerticalAlignment = getVerticalAlign(alignProperty.verticalAlign)
		}
	}

	private fun updateSize() {
		component.node.setSize(textBlock.widthInPixels.toDouble(), textBlock.heightInPixels.toDouble())
	}

	private fun getHorizontalAlign(dy: Align): Number {
		if (dy.factor < 0.33) {
			return Control.HORIZONTAL_ALIGNMENT_LEFT
		} else if (dy.factor < 0.66) {
			return Control.HORIZONTAL_ALIGNMENT_CENTER
		} else {
			return Control.HORIZONTAL_ALIGNMENT_RIGHT
		}
	}

	private fun getVerticalAlign(dx: Align): Number {
		if (dx.factor < 0.33) {
			return Control.VERTICAL_ALIGNMENT_TOP
		} else if (dx.factor < 0.66) {
			return Control.VERTICAL_ALIGNMENT_CENTER
		} else {
			return Control.VERTICAL_ALIGNMENT_BOTTOM
		}
	}
}